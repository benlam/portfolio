#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

// MACRO DE DEBBUGGAGE
#define LOGGING 0 // 0 : NORMAL MODE | 1 : LOGGING MODE
#if LOGGING
#define LOG(code) code
#else
#define LOG(code)
#endif

#define PROMPT "aash$ "

//------------------------------------------------------------------------------
int isSpace(char c);
void splitArg(char* line, int argcMax, char* argv[]);
void printArg(char* argv[]);
int findAmp(char* argv[]);
void handleChld(int sig);
int readLine(char* line, int linesize);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int main(int argc, char* argv[])
//------------------------------------------------------------------------------
{
    LOG(printf("---  LOGGING MODE ! (change the LOGGING token value in main.c "
            "directives into '0' to be in Normal mode)  ---\n"));
    LOG(printf("%s()\n", __func__));
    LOG(if (argc>1) printf("Called arguments :\n"));
    LOG(if (argc>1) printArg(argv));

    int run = 1;

    const unsigned linesize = 1024;
    char line[linesize];

    // La shell "tourne" tant que la commande exit n'est pas saisie
    // et tant que EOF n'a pas été saisi
    while(run && (readLine(line, linesize) != 0)){

        LOG(printf("Ligne lue : %s\n", line));

        // Éclatement de la saisie en un tableau d'arguments
        const int argcMax = 128;
        char* argvCmd[argcMax];

        splitArg(line, argcMax, argvCmd);

        LOG(printArg(argvCmd));

        // On execute alors la commande représentée par le premier argument
        if(strcmp(argvCmd[0], "exit") == 0){
            // Cas d'une commande "exit"

            run = 0; // Entraîne la fin de la boucle d'execution

        }else{
            // Tous les autres cas : on crée un autre processus

            LOG(printf("Fork du processus n°%d en cours...\n", getpid()));

            // On teste la présence de l'esperluette dans la commande
            int amp = findAmp(argvCmd);

            // On crée le nouveau processus
            pid_t p = fork();
            // À partir d'ici, deux processus s'exécutent.

            if(p == 0){
                // Cette partie de code est réservée au processus fils
                LOG(printf("Exécution processus fils n°%d\n", getpid()));

                // On transforme le processus fils en un processus correspondant
                // à la commande représentée par l'argument n°0 du tableau.
                // Les arguments à passer à ce nouveau processus transformé sont
                // contenus dans le même tableau d'arguments.
                execvp(argvCmd[0], argvCmd);

                // La suite n'est exécutée que si execvp a échoué
                LOG(printf("Échec\n"));

                printf("%s command not found\n", argvCmd[0]);

                // Le processus fils qui n'a pas réussi à exécuter sa commande
                // se termine en notifiant une erreur dans sa terminaison
                return EXIT_FAILURE;

            }else{
                // Cette partie de code est réservée à notre shell
                LOG(printf("Exécution du processus père n°%d\n", getpid()));

                // Le père se met à l'écoute des signaux et indique que la
                // méthode handleChld() implémente la gestion des signaux
                struct sigaction sig;
                sigemptyset(&sig.sa_mask); // Pas de signal à bloquer
                sig.sa_flags = 0; // Comportement normal du handler

                sig.sa_handler = handleChld;
                sigaction(SIGCHLD, &sig, NULL);

                // Le shell ne se met en attente de ses fils que si
                // l'esperluette n'est pas détectée dans les paramètres de la commande
                if(!amp) wait(NULL);
            }
        }
    }

    printf("You are leaving aash ... bye\n");
    LOG(printf("Fin du processus 'main' (%d)\n", getpid()));

    return EXIT_SUCCESS;
}

/**
 * Teste si le caractère c est un caractère d'espacement. 'True' si c est un ' '
 * , une tabulation, un line-feed ou une fin de chaîne.
 * @param c le caractère à tester
 * @return le résultat du test
 */
//------------------------------------------------------------------------------
int isSpace(char c)
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));

    return (c == ' ' || c == '\t' || c == '\n' || c == '\0');
}

/**
 * Éclate la chaîne line en un tableau de chaînes argv. Argv contient donc les
 * arguments éclatés. argv contient argcMax arguments maximum.
 * @param line la chaîne à éclater
 * @param argcMax le nombre maximum d'argument dans argv
 * @param argv le tableau de paramètres éclatés
 */
//------------------------------------------------------------------------------
void splitArg(char* line, int argcMax, char* argv[])
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));;

    // À chaque pas, on stockera le caractère en cours et le précédent.
    char previousChar;
    char currentChar;

    // Compteur de caractères dans line
    int charCount = 0;

    // Compteur de mots trouvés
    int wordCount = 0;

    int lineLength = strlen(line);

    LOG(printf("lineLength : %d\n", lineLength));

    // On parcours toute la saisie 'line', caractère par caractère
    while(charCount < lineLength){

        // Au premier tour, on agit comme previousChar était un ' '
        if(charCount == 0){
            previousChar = ' ';
        }else{
            previousChar = line[charCount-1];
        }

        currentChar = line[charCount];

        LOG(printf("previousChar : %c | currentChar : %c\n", previousChar, currentChar));

        if(isSpace(previousChar) && !isSpace(currentChar)){
            // Début de mot détecté
            LOG(printf("Début de mot !\n"));

            // On vérifie que le maximum d'arguments n'a pas été atteint...
            if(wordCount < argcMax){
                // ... si non : on stocke l'adresse du début de mot
                argv[wordCount] = &line[charCount];
                wordCount++;
            }else{
                // ... si oui : on ne fait rien.
                printf("Arguments limit reached (%d)\n", argcMax-1); // Le dernier élément d'argv devant être '\0'
            }

        }else if(!isSpace(previousChar) && isSpace(currentChar)){
            // Fin de mot détectée. On remplace l'espace par un fin de chaîne.
            LOG(printf("Fin de mot !\n"));
            line[charCount] = '\0';
        }

        charCount++;
    }

    LOG(printf("Fin de l'éclatement des arguments\n"));

    // On cloture le tableau d'arguments avec un pointeur NULL.
    argv[wordCount] = NULL;
}

/**
 * Affiche en sortie standard la liste des arguments contenus dans un tableau
 * de chaines de caractères.
 * @param argv le tableau de chaînes de caractères à afficher
 */
//------------------------------------------------------------------------------
void printArg(char* argv[])
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));

    for(int i = 0 ; argv[i] != NULL ; i++)
        printf("arg n°%d : %s\n", i, argv[i]);
}

/**
 * Détecte la présence de l'esperluette dans un tableau de paramètres.
 * Si l'esperluette est détectée, la fonction retourne 1 et retire l'esperluette
 * des paramètres. Si elle n'est pas détectée, la fonction retourne 0.
 * @param argv le talbeau de paramètre dans lequel cherche l'esperluette
 * @return le resultat du test de présence
 */
//------------------------------------------------------------------------------
int findAmp(char* argv[])
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));

    // On commence par compter les arguments passés
    int argc=0;

    while(argv[argc] != NULL){
        argc++;
    }

    LOG(printf("%d argument(s) trouvé(s)\n", argc));
    LOG(printf("Dernier argument : %s\n", argv[argc-1]));

    // On mesure la taille du dernier argument (qui contient peut-être '&')
    int argLength = strlen(argv[argc-1]);

    LOG(printf("Taille du dernier argument : %d\n", argLength));

    // On récupère le dernier caractère du dernier argument.
    char lastChar = argv[argc-1][argLength-1];

    // On teste si ce caractère est l'esperluette...
    if(lastChar == '&'){
        // ... si oui : on retourne 1 (true) et on retire l'esperluette
        LOG(printf("Esperluette trouvée\n"));

        argv[argc-1][argLength-1] = '\0';
        return 1;
    }else{
        //... si non : on retourne 0 (false)
        LOG(printf("Esperluette absente\n"));

        return 0;
    }
}

/**
 * Gère les signaux IPC reçus de la part d'autres processus.
 * @param sig le code signal envoyé par le processus appelant
 */
//------------------------------------------------------------------------------
void handleChld(int sig)
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));

    pid_t p;
    int status;

    // Si le signal provient d'un processus que le père n'attendait pas,
    // on affiche son pid.
    while((p = waitpid(-1, &status, WNOHANG)) != -1){
        printf("[SIGCHLD handler : Process n°%d exited]\n", p);
    }
}

/**
 * Lit l'entrée standard de façon sécurisée et gère EOF. Si la saisie est
 * correcte (ni erreur, ni EOF), la fonction retourne 1. Sinon, retourne 0.
 * @param line le buffer de saisie
 * @param linesize la taille maximale du buffer
 * @return le resultat de la saisie sécurisée
 */
//------------------------------------------------------------------------------
int readLine(char* line, int linesize)
//------------------------------------------------------------------------------
{
    LOG(printf("%s()\n", __func__));

    char* resultLine = NULL;

    do{
        // Affichage du prompt
        printf("%s", PROMPT);

        // Saisie
        resultLine = fgets(line, linesize, stdin);

        if(feof(stdin)){
            printf("exit\n");
            return 0;
        }else if(ferror(stdin)){
            sleep(1);
        }
    }while(resultLine == NULL);

    return 1;
}
