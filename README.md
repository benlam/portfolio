# PORTFOLIO #
*(en travaux)*  
En fouillant mes dossiers de «source», vous trouverez quelques exemples de mes travaux liés à la programmation.  

## C ##
1. **AASH**  
Squelette d'interprêteur Shell UNIX.  
À la manière d'un interpréteur de la famille «Bourne», Aash s'exécute en ligne de commande et interprête vos commandes UNIX.  
Déjà implémenté :  
   - lecture sécurisée de la saisie  
   - terminaison lors de la saisie d'une fin de fichier EOF (Ctrl + D)  
   - détection d'un paramètre «esperluette» pour signifier l'exécution dans un processus fils  
   - création d'un processus fils via appel système `fork()`  
   - mutation des données d'exécution du processus fils via appel système `execvp()` (exécution de la commande)  
   - détection des signaux IPC d'interruption des processus fils  
   - retransmission des paramètres de commande saisis  

## C++ ##
*À venir*  

## Java ##
1. **Oracle Certified Programmer - Java Programmer**  
(Documents justificatifs sur demande)  

## Web HTML/CSS/PHP/JS ##
*À venir*  

## Mon CV ##
*Tout est dans le titre*  

**N.B. :** *Tous les fichiers et extraits de codes présentés dans ce dépôt sont libres pour la réutilisation. Servez-vous ! ;)*  
